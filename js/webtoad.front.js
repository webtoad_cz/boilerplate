/**
 * WebToad Front-end Scripts
 *
 * @copyright Copyright (c) 2011-2014 WebToad s.r.o.
 */

$().ready(function() {

    // Nahrazeni svg obrazku obrazkem umistenym podle atributu data-fallback ===
    if (!Modernizr.svg) {
        $("img[src$='.svg']").each(function(index, element){
            var fallback = $(this).data('fallback');
            $(this).attr('src', fallback);
        })
    }

    // Placeholder fallback ====================================================
    if(!Modernizr.input.placeholder){

        $('[placeholder]').focus(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
        }).blur(function() {
                var input = $(this);
                if (input.val() == '' || input.val() == input.attr('placeholder')) {
                    input.addClass('placeholder');
                    input.val(input.attr('placeholder'));
                }
            }).blur();
        $('[placeholder]').parents('form').submit(function() {
            $(this).find('[placeholder]').each(function() {
                var input = $(this);
                if (input.val() == input.attr('placeholder')) {
                    input.val('');
                }
            })
        });
    }

    /**
     * Height equalizer
     * @param elements Elements to equalize
     * @param step 8 elements, step 3 => 1 2 3 equalized, 4 5 6 equalized, 7 8 equalized
     */
    function equalizeHeight(elements, step){

        function eq(elements) {

            // Find max height
            var maxHeight = 0;

            $(elements).each(function(index, element){
                var elHeight = $(element).height();
                if(elHeight > maxHeight){
                    maxHeight = elHeight;
                }
            })

            // Set max height
            $(elements).each(function(index, element){
                $(element).height(maxHeight);
            })
        }

        // Splitting
        if(step) {
            var $elements = $(elements);
            var elsize = $elements.size();
            var stop = ($elements.size()%step) > 0 ? ((Math.floor($elements.size()/step)) + 1)*step : (Math.floor($elements.size()/step))*step;
            for(var i = 0; i < stop; i+=step) {
                if(i + step > elsize) {
                    eq($elements.slice(i, elsize));
                }
                else {
                    eq($elements.slice(i, i + step));
                }
            }
        }
        else {
            eq(elements);
        }
    }


});