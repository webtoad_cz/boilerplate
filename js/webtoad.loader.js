/**
 * WebToad Javascript Loader
 *
 * @copyright Copyright (c) 2011-2014 WebToad s.r.o.
 */

/**
 * Prereserved constants
 */
var $body = document.getElementsByTagName('body')[0];
var $basePath = $body.getAttribute("data-basepath");

Modernizr.load([
    {
        // Load jQuery from Google repository. If it's not available fallback to local jQuery file.
        load: 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js',
        complete: function () {
            if (!window.jQuery) {
                Modernizr.load($basePath + 'js/vendor/jquery-1.11.0.min.js');
            }
        }
    },
    {
        // Load all other libraries here
        load: [
            //[example] $basePath + 'js/easing.js?v=0.1',
            //[example] $basePath + 'js/jquery.scrollTo-1.4.3.1-min.js?v=0.1',
        ],
        complete: function () {
            // Initialize jQuery plugins or libraries that will be needed in webtoad.front.js.
            //[example] $.nette.init();
            //[example] $.webtoad.init();
        }
    },
    {
        // Load webtoad.front.js (It should load after all other libraries were loaded.)
        load: $basePath + 'js/webtoad.front.js'
    }
]);